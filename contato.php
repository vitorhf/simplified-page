<?php
foreach($_GET as $k=>$v) {
	${$k} = $v;
}
foreach($_POST as $k=>$v) {
	${$k} = $v;
}
date_default_timezone_set('America/Sao_Paulo');
$date = date('d/m/Y');
$nomeArq3 = "arq/contatos.txt";

if (isset($oper)) {
	if ($oper == 'salvarContato') {
		if (trim($nome) != '') {
			$fp = fopen($nomeArq3,"a");
			fwrite($fp, $nome.'|'.$telefone."\n");
			fclose($fp);
		}
	} else if ($oper == 'excluirContato') {
		$dados = file($nomeArq3);
		foreach($dados as $v) {
			$v_ori = $v;
			$v = str_replace("\r",'',str_replace("\n",'',str_replace("<BR>","",$v)));
			$reg = str_replace("\n",'',str_replace("<BR>","",$reg));
			if (trim($v) == trim($reg)) {
				continue;
			}
			$new[] = $v_ori;
		}
		$fp = fopen($nomeArq3,"w+");
		if (is_array($new) and count($new) > 0) {
			foreach($new as $v) {
				fwrite($fp, $v);
			}
		}
		fclose($fp);
	}
}

$table3 = '';


$fp = fopen($nomeArq3,"r");
//Lê o conteúdo do arquivo aberto.
while (!feof ($fp)) {
	$dados = fgets($fp, 4096);
	if (trim($dados) != '') {
		$d = explode('|',$dados);
		$table4 .= '<tr>';
		$table4 .= '<td>'.$d[0].'</td>';
		$table4 .= '<td>'.$d[1].'</td>';
		$table4 .= '<td><a href="contato.php?oper=excluirContato&reg='.str_replace("<BR>","",$dados).'" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>';
		$table4 .= '</tr>';
	}
}


include_once("top.php");
include_once("header.php");
?>
      <!-- Full Width Column -->
      <div class="content-wrapper">
        <div class="container">
          <!-- Main content -->
          <section class="content">
			<div class="row">
			   <div class="col-md-12">
					<div class="box">
						<section class="content-header">
							<h1>
							Contatos<button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#modal-success"><i class="fa fa-plus" aria-hidden="true"></i></button>
							</h1>
						</section>
						<div class="box-body">
							<table id="example4" class="table table-bordered table-hover stripe row-border hover" cellspacing="0" width="100%">
							<thead>
							  <tr>
								<th width="60%"><B>NOME</B></th>
								<th width="30%"><B>TELEFONE/RAMAL</B></th>
								<th><B>EXCLUIR</B></th>
							  </tr>
							</thead>
							<tbody>
							<?=$table4?>
							</tbody>
							</table>
						</div>
					</div>
				</div>
			 </div>
	      </section>
        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->
	  <div class="modal fade" id="modal-success">
          <div class="modal-dialog">
            <div class="modal-content">
			  <form action="index.php" method="POST">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">×</span></button>
					<h4 class="modal-title">Adicionar Contato</h4>
				  </div>
				  <div class="modal-body">

						<div class="box-body">
							<div class="form-group">
								<label for="nome" class="col-sm-4 control-label" style="top:7px">Nome: </label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="nome" name="nome">
								</div>
								<label for="telefone" class="col-sm-4 control-label" style="top:7px">Telefone: </label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="telefone" name="telefone">
								</div>
								<input type="hidden" id="oper" name="oper" value="salvarContato">
							</div>
						</div><!-- /.box-body -->
				  </div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Salvar</button>
				  </div>
			  </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<?php
include_once("footer.php");
include_once("script_footer.php");
?>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function() {
	var table = $('#example4').DataTable( {
		"pageLength": 10 ,
		"language": {
				"sEmptyTable": "Nenhum registro encontrado",
				"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
				"sInfoEmpty": "Mostrando 0 até de 0 registros",
				"sInfoFiltered": "(Filtrados de _MAX_ registros)",
				"sInfoPostFix": "",
				"sInfoThousands": ".",
				"sLengthMenu": "_MENU_ resultados por página",
				"sLoadingRecords": "<img src='../img/carregando.gif'>",
				"sProcessing": "Processando...",
				"sZeroRecords": "Nenhum registro encontrado",
				"sSearch": "Pesquisar",
				"oPaginate": {
					"sNext": "Próximo",
					"sPrevious": "Anterior",
					"sFirst": "Primeiro",
					"sLast": "Último"
				},
				"oAria": {
					"sSortAscending": ": Ordenar colunas de forma ascendente",
					"sSortDescending": ": Ordenar colunas de forma descendente"
				}
		}
	});
});

</script>
<?php
include_once("bottom.php");
?>


