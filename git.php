<?php
include_once("top.php");
include_once("header.php");
$d = [
    ['git init','Cria um novo repositório'],
    ['git clone LinkRepositorio','Clona o repositório'],
    ['git add nomeArquivo (ou . para todos)','Adiciona os arquivos alterados para commit'],
    ['git commit -m "comentários da alteração"','Confirma as alterações, adicionando comentário sobre o mesmo.'],
    ['git checkout -b nomeBranch','Cria uma nova branch e entra na mesma'],
    ['git checkout nomeBranch','Entra na branch'],
    ['git branch -D nomeBranch','Apaga a branch localmente'],
    ['git push nomeOrigin nomeBranch --delete','Apaga a branch remotamente'],
    ['git pull','Atualiza o repositório local para a versão mais recente da branch'],
    ['git merge nomeBranch','Efetua merge da branch informada na sua branch ativa'],
    ['git diff branchOrigem branchDestino','Mostra diferenças entre as branchs'],
    ['git log','Mostra o histório de commits da branch ativa'],
    ['git checkout -- nomeArquivo (ou .)','Ignora as alterações feitas'],
    ['git fetch origin','Atualiza as referências locais com relação às remotas'],
    ['git status','Verifica o status dos arquivos alterados'],
    ['git rm nomeArquivo','Remove o arquivo'],
    ['git reset HEAD nomeArquivo','Desfaz alteração local quando o arquivo já foi adicionado na staged area'],
    ['git remote','Exibe os repositórios remotos'],
    ['git push','Envia os arquivos commitados para o repositório remoto'],
    ['git branch','Lista as branchs'],
    ['git rebase','Efetua merge da branch informada na sua mas de forma diferente do git merge. <a href="https://medium.com/vtex-lab/por-que-voc%C3%AA-deveria-usar-git-rebase-d75b41e900f2" target="_blank">Veja aqui</a>'],
    ['git stash (apply)','Adiciona os arquivos alterados para uma lista temporária. Usado normalmente quando não deseja commitar arquivos alterados e precisa mudar de branch.']
];

//Lê o conteúdo do arquivo aberto.
foreach($d as $v) {
	$table .= '<tr>';
	$table .= '<td>'.$v[0].'</td>';
	$table .= '<td>'.$v[1].'</td>';
	$table .= '</tr>';
}

?>
<!-- Full Width Column -->
      <div class="content-wrapper">
        <div class="container">
          <!-- Main content -->
          <section class="content">
			<div class="row">
				<div class="col-md-12" id="divResult">
					<div class="box">
						<section class="content-header">
							<h1>
							GIT<button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#modal-todo"><i class="fa fa-plus" aria-hidden="true"></i></button>
							</h1>
						</section>
						<div class="box-body">
							<table id="example2" class="display cell-border compact stripe hover table table-striped table-bordered" cellspacing="0" width="100%">
							<thead>
							  <tr>
								<th><B>COMANDO</B></th>
								<th><B>DESCRIÇÃO</B></th>
							  </tr>
							</thead>
							<tbody>
							<?=$table?>
							</tbody>
							</table>
						</div>
					</div>
			   </div>
			 </div>
	      </section>
        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->
<?php
include_once("footer.php");
include_once("script_footer.php");
?>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function() {


	var table = $('#example2').DataTable( {
		"pageLength": 50,
		"language": {
				"sEmptyTable": "Nenhum registro encontrado",
				"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
				"sInfoEmpty": "Mostrando 0 até de 0 registros",
				"sInfoFiltered": "(Filtrados de _MAX_ registros)",
				"sInfoPostFix": "",
				"sInfoThousands": ".",
				"sLengthMenu": "_MENU_ resultados por página",
				"sLoadingRecords": "<img src='../img/carregando.gif'>",
				"sProcessing": "Processando...",
				"sZeroRecords": "Nenhum registro encontrado",
				"sSearch": "Pesquisar",
				"oPaginate": {
					"sNext": "Próximo",
					"sPrevious": "Anterior",
					"sFirst": "Primeiro",
					"sLast": "Último"
				},
				"oAria": {
					"sSortAscending": ": Ordenar colunas de forma ascendente",
					"sSortDescending": ": Ordenar colunas de forma descendente"
				}
		}
	});

});

</script>
<?php
include_once("bottom.php");
?>
