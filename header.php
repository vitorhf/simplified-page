  </head>
  <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
  <body class="hold-transition skin-blue layout-top-nav">
    <div class="wrapper">
      <header class="main-header">
        <nav class="navbar navbar-static-top">
          <div class="container">
            <div class="navbar-header">
		          <!--<a href="index.php" class="navbar-brand"><img src="dist/images/logo_blue.svg" style="width: 189px;margin-top: -11px;"></a>-->
		          <a href="index.php" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>SP</b></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg">Simplified Page</span>
              </a>
              <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                <ul class="nav navbar-nav">
                  <li><a href="index.php"><i class="fa fa-check-square-o "></i> <span>Home</span></a></li>
				          <li><a href="contato.php"><i class="fa fa-user "></i> <span>Contatos</span></a></li>
				          <li><a href="calendar.php"><i class="fa fa-calendar "></i> <span>Calendario</span></a></li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gears "></i> <span>Suporte</span><span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="git.php"> <span>Git</span></a></li>
                      <li><a href="lando.php"> <span>Lando</span></a></li>
                      <li><a href="ambiente.php"> <span>Refazer ambiente</span></a></li>
                      <li><a href="mergeRelease.php"> <span>Merge Release pós conflito</span></a></li>
                    </ul>
                  </li>
				          <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-mobile "></i> <span>Aplicativos</span> <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="salario.php"> <span>Calcular Salário</a></li>
					            <li><a href="ferias.php"> <span>Calcular Férias</a></li>
					            <li><a href="inverter.php"> <span>Inverter</a></li>
					            <li><a href="base64Decoder.php"> <span>Base64 Decode</a></li>
                    </ul>
                  </li>
                  <li><a href="https://meet.google.com/" target="_blank">Meet</a></li>
                  <!--<li><a href="https://meet.google.com/tmv-sjsa-cci?hs=122" target="_blank" rel="noreferrer noopener" class="goktNc" aria-label="Join Hangouts Meet (wrh-kvay-pfo)">Daily</a></li>-->
                </ul>
              </div>
            </div>
          </div>
        </nav>
      </header>

