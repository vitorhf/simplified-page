<?php
include_once("top.php");
?>
<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="dist/css/jquery.dataTables.min.css">
<style>
.destaque {
	background-color: #9ccfec !important;
}
</style>
<?php
include_once("header.php");
?>
      <!-- Full Width Column -->
      <div class="content-wrapper">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              Cálculo de Salário
            </h1>
          </section>

          <!-- Main content -->
          <section class="content">
			<form role="form" NAME="form1">
            <div class="row">
               <div class="col-md-12">
			      <div class="box-body">
					<div class="form-group">
                      <label for="salario" class="col-sm-2 control-label" style="top:7px">Salário Bruto:</label>
                      <div class="col-sm-4">
						<input type="number" min="953.00" max="100000.00" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control" id="salario" name="salario">
                      </div>
                    </div>
				  </div>
				</div>
			   </div>
			   <div class="row">
                 <div class="col-md-12">
			       <div class="box-body">
					 <div class="form-group">
                      <label for="qtdeDependentes" class="col-sm-2 control-label" style="top:7px">Qtde Dependentes:</label>
                      <div class="col-sm-4">
                        <input type="number" class="form-control" id="qtdeDependentes" name="qtdeDependentes">
                      </div>
                    </div>
                  </div><!-- /.box-body -->
			   </div>
			  </div>
			  <div class="row">
                 <div class="col-md-12">
			       <div class="box-body">
					<div class="form-group">
                      <button type="button" class="btn btn-primary" id="btnCalcular" name="btnCalcular">Calcular</button>
                    </div>
                  </div><!-- /.box-body -->
			   </div>
			  </div>
			  </form>
			<div class="row" id="divResult" style="display:none;">
				<div class="col-md-6">
				  <!-- Widget: user widget style 1 -->
				  <div class="box box-widget widget-user-2">
					<div class="box-footer no-padding">
					  <ul class="nav nav-stacked" style="font-size: 18px">
						<li>Salário Bruto <span class="pull-right" id="salarioBruto"></span></li>
						<li>INSS <span class="pull-right " id="inss"></span></li>
						<li>IRRF <span class="pull-right " id="irrf"></span></li>
						<li>Salário Líquido <span class="pull-right " id="salarioliquido"></span></li>
					  </ul>
					</div>
				  </div>
				  <!-- /.widget-user -->
				</div>
				<div class="col-md-6">
				  <!-- Widget: user widget style 1 -->
				  <div class="box box-widget widget-user-2">
					<div class="box-footer no-padding">
					  <ul class="nav nav-stacked" style="font-size: 18px">
						<li>FGTS Mensal <span class="pull-right" id="fgtsMensal"></span></li>
						<li>FGTS Anual <span class="pull-right " id="fgtsAnual"></span></li>
						<li>Salário Bruto Anual* <span class="pull-right " id="brutoAnual"></span></li>
						<li>Salário Líquido Anual* <span class="pull-right " id="liquidoAnual"></span></li>
					  </ul>
					</div>
					* Não inclui 13º e férias
				  </div>
				  <!-- /.widget-user -->
				</div>
			</div>
	      </section>
        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->
<?php
include_once("footer.php");
include_once("script_footer.php");
?>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function() {

	$("#btnCalcular").click(function() {
		var salario = $('#salario').val();
		var qtdeDependentes = $('#qtdeDependentes').val();
		$.ajax({
			url: 'calculoSalario.php',
			method: 'POST',
			data:  {'salario': salario, 'qtdeDependentes': qtdeDependentes},
			dataType: "json",
			success: function(r){
				$("#salarioBruto").text(r.salarioBruto);
				$("#inss").text(r.INSS);
				$("#irrf").text(r.IRRF);
				$("#salarioliquido").text(r.salarioLiquido);
				$("#fgtsMensal").text(r.fgtsMensal);
				$("#fgtsAnual").text(r.fgtsAnual);
				$("#brutoAnual").text(r.brutoAnual);
				$("#liquidoAnual").text(r.liquidoAnual);
				$('#divResult').show();
			},
			
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				alert('Erro ao calcular!');
				console.log(textStatus);
				console.log(XMLHttpRequest);
				console.log(errorThrown);
			}
		});
	});
});

</script>
<?php
include_once("bottom.php");
?>      


