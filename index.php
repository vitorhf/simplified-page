<?php
foreach($_GET as $k=>$v) {
	${$k} = $v;
}
foreach($_POST as $k=>$v) {
	${$k} = $v;
}
date_default_timezone_set('America/Sao_Paulo');
$date = date('d/m/Y');
$nomeArq = "arq/task.txt";
$nomeArq2 = "arq/dia.txt";
$nomeArq4 = "arq/branchs.txt";
if (isset($oper)) {
	if ($oper == 'salvar') {
		if (trim($descricao) != '') {
			$descricao = str_replace("\n","<BR>",$descricao);
			$fp = fopen($nomeArq,"a");
			fwrite($fp, $date.'|'.$descricao."\n");
			fclose($fp);
		}
	} else if ($oper == 'excluir') {
		$dados = file($nomeArq);
		foreach($dados as $v) {
			$v_ori = $v;
			$v = str_replace("\r",'',str_replace("\n",'',str_replace("<BR>","",$v)));
			$reg = str_replace("\n",'',str_replace("<BR>","",$reg));
			if (trim($v) == trim($reg)) {
				continue;
			}
			$new[] = $v_ori;
		}
		$fp = fopen($nomeArq,"w+");
		if (is_array($new) and count($new) > 0) {
			foreach($new as $v) {
				fwrite($fp, $v);
			}
		}
		fclose($fp);
	} else if ($oper == 'salvarDia') {
		if (trim($descricaoDia) != '') {
			$descricaoDia = str_replace("\n","<BR>",$descricaoDia);
			$fp = fopen($nomeArq2,"a");
			fwrite($fp, $date.'|'.$descricaoDia."\n");
			fclose($fp);
		}
	} else if ($oper == 'excluirDia') {
		$dados = file($nomeArq2);
		foreach($dados as $v) {
			$v_ori = $v;
			$v = str_replace("\r",'',str_replace("\n",'',str_replace("<BR>","",$v)));
			$reg = str_replace("\n",'',str_replace("<BR>","",$reg));
			if (trim($v) == trim($reg)) {
				continue;
			}
			$new[] = $v_ori;
		}
		$fp = fopen($nomeArq2,"w+");
		if (is_array($new) and count($new) > 0) {
			foreach($new as $v) {
				fwrite($fp, $v);
			}
		}
		fclose($fp);
	} else if ($oper == 'salvarBranch') {
		if (trim($branch) != '') {
			$fp = fopen($nomeArq4,"a");
			$descricaoBranch = str_replace("\n","<BR>",$descricaoBranch);
			fwrite($fp, $branch.'|'.$descricaoBranch."\n");
			fclose($fp);
		}
	} else if ($oper == 'excluirBranch') {
		$dados = file($nomeArq4);
		foreach($dados as $v) {
			$v_ori = $v;
			$v = str_replace("\r",'',str_replace("\n",'',str_replace("<BR>","",$v)));
			$reg = str_replace("\n",'',str_replace("<BR>","",$reg));
			if (trim($v) == trim($reg)) {
				continue;
			}
			$new[] = $v_ori;
		}
		$fp = fopen($nomeArq4,"w+");
		if (is_array($new) and count($new) > 0) {
			foreach($new as $v) {
				fwrite($fp, $v);
			}
		}
		fclose($fp);
	}
}

$table = $table4 = $table5 = '';
// pegar as tarefas
$fp = fopen($nomeArq,"r");
//Lê o conteúdo do arquivo aberto.
while (!feof ($fp)) {
	$dados = fgets($fp, 4096);
	if (trim($dados) != '') {
		$tarefa = explode('|',$dados);
		$table .= '<tr>';
		$table .= '<td>'.$tarefa[0].'</td>';
		$table .= '<td>'.$tarefa[1].'</td>';
		$table .= '<td><a href="index.php?oper=excluir&reg='.str_replace("<BR>","",$dados).'" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>';
		$table .= '</tr>';
	}
}

// pegar as tarefas
$fp = fopen($nomeArq2,"r");
//Lê o conteúdo do arquivo aberto.
while (!feof ($fp)) {
	$dados = fgets($fp, 4096);
	if (trim($dados) != '') {
		$d = explode('|',$dados);
		$table3 .= '<tr>';
		$table3 .= '<td>'.$d[0].'</td>';
		$table3 .= '<td>'.$d[1].'</td>';
		$table3 .= '<td><a href="index.php?oper=excluirDia&reg='.str_replace("<BR>","",$dados).'" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>';
		$table3 .= '</tr>';
	}
}
$fp = fopen($nomeArq4,"r");
//Lê o conteúdo do arquivo aberto.
while (!feof ($fp)) {
	$dados = fgets($fp, 4096);
	if (trim($dados) != '') {
		$d = explode('|',$dados);
		$table5 .= '<tr>';
		$table5 .= '<td>'.$d[0].'</td>';
		$table5 .= '<td>'.$d[1].'</td>';
		$table5 .= '<td><a href="index.php?oper=excluirBranch&reg='.str_replace("<BR>","",$dados).'" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>';
		$table5 .= '</tr>';
	}
}

include_once("top.php");
include_once("header.php");
?>
      <!-- Full Width Column -->
      <div class="content-wrapper">
        <div class="container">
          <!-- Main content -->
          <section class="content">
			<div class="row">
				<div class="col-md-6" id="divResult">
					<div class="box">
						<section class="content-header">
							<h1>
							ToDo List<button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#modal-todo"><i class="fa fa-plus" aria-hidden="true"></i></button>
							</h1>
						</section>
						<div class="box-body">
							<table id="example2" class="table table-bordered table-hover stripe row-border hover" cellspacing="0" width="100%">
							<thead>
							  <tr>
								<th><B>DATA</B></th>
								<th><B>TAREFA</B></th>
								<th><B>EXCLUIR</B></th>
							  </tr>
							</thead>
							<tbody>
							<?=$table?>
							</tbody>
							</table>
						</div>
					</div>
			   </div>
			   <div class="col-md-6"  id="divResult2">
					<div class="box">
						<section class="content-header">
							<h1>
							O que fez no dia?<button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#modal-hj"><i class="fa fa-plus" aria-hidden="true"></i></button>
							</h1>
						</section>
						<div class="box-body">
							<table id="example3" class="table table-bordered table-hover stripe row-border hover" cellspacing="0" width="100%">
							<thead>
							  <tr>
								<th><B>DATA</B></th>
								<th><B>DESCRIÇÃO</B></th>
								<th><B>EXCLUIR</B></th>
							  </tr>
							</thead>
							<tbody>
							<?=$table3?>
							</tbody>
							</table>
						</div>
					</div>
				 </div>
</div>
<div class="row">
			   <div class="col-md-6">
					<div class="box">
						<section class="content-header">
							<h1>
							Branchs<button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#modal-branch"><i class="fa fa-plus" aria-hidden="true"></i></button>
							</h1>
						</section>
						<div class="box-body">
							<table id="example5" class="table table-bordered table-hover stripe row-border hover" cellspacing="0" width="100%">
							<thead>
							  <tr>
								<th><B>BRANCH</B></th>
								<th><B>DESCRIÇÃO</B></th>
								<th><B>EXCLUIR</B></th>
							  </tr>
							</thead>
							<tbody>
							<?=$table5?>
							</tbody>
							</table>
						</div>
					</div>
				</div>
				<!--
				<div class="col-md-6">
				<div class="box">
						<section class="content-header">
							<h1>
							Links
							</h1>
						</section>
						<div class="box-body">
							<table id="example5" class="table table-bordered table-hover stripe row-border hover" cellspacing="0" width="100%">
							<thead>
							  <tr>
								<th><B>DESCRIÇÃO</B></th>
								<th><B>LINK</B></th>
							  </tr>
							</thead>
							<tbody>
								<tr><td>Reuniões (Meet)</td><td><a href="https://meet.google.com/" target="_blank">Link</a></td></tr>
							</tbody>
							</table>
						</div>
					</div>
				 </div>
-->
			 </div>
	      </section>
        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->
	  <div class="modal fade" id="modal-todo">
          <div class="modal-dialog">
            <div class="modal-content">
			  <form action="index.php" method="POST">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">×</span></button>
					<h4 class="modal-title">Adicionar Tarefa</h4>
				  </div>
				  <div class="modal-body">

						<div class="box-body">
							<div class="form-group">
								<label for="descricao" class="col-sm-2 control-label" style="top:7px">Descrição: </label>
								<div class="col-sm-10">
									<textarea class="form-control" id="descricao" name="descricao"></textarea>
								</div>
								<input type="hidden" id="oper" name="oper" value="salvar">
							</div>
						</div><!-- /.box-body -->
				  </div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Salvar</button>
				  </div>
			  </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
		<div class="modal fade" id="modal-hj">
          <div class="modal-dialog">
            <div class="modal-content">
			  <form action="index.php" method="POST">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">×</span></button>
					<h4 class="modal-title">O que fez hoje?</h4>
				  </div>
				  <div class="modal-body">

						<div class="box-body">
							<div class="form-group">
								<label for="descricaoDia" class="col-sm-2 control-label" style="top:7px">Descrição: </label>
								<div class="col-sm-10">
									<textarea class="form-control" id="descricaoDia" name="descricaoDia"></textarea>
								</div>
								<input type="hidden" id="oper" name="oper" value="salvarDia">
							</div>
						</div><!-- /.box-body -->
				  </div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Salvar</button>
				  </div>
			  </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
		<div class="modal fade" id="modal-branch">
          <div class="modal-dialog">
            <div class="modal-content">
			  <form action="index.php" method="POST">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">×</span></button>
					<h4 class="modal-title">Adicionar Branch</h4>
				  </div>
				  <div class="modal-body">

						<div class="box-body">
							<div class="form-group">
								<label for="branch" class="col-sm-4 control-label" style="top:7px">Branch: </label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="branch" name="branch">
								</div>
								<label for="descricaoBranch" class="col-sm-4 control-label" style="top:7px">Descrição: </label>
								<div class="col-sm-8">
									<textarea class="form-control" id="descricaoBranch" name="descricaoBranch"></textarea>
								</div>
								<input type="hidden" id="oper" name="oper" value="salvarBranch">
							</div>
						</div><!-- /.box-body -->
				  </div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Salvar</button>
				  </div>
			  </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<?php
include_once("footer.php");
include_once("script_footer.php");
?>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function() {

/*
	var table = $('#example2').DataTable( {
		"pageLength": 10,
		"language": {
				"sEmptyTable": "Nenhum registro encontrado",
				"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
				"sInfoEmpty": "Mostrando 0 até de 0 registros",
				"sInfoFiltered": "(Filtrados de _MAX_ registros)",
				"sInfoPostFix": "",
				"sInfoThousands": ".",
				"sLengthMenu": "_MENU_ resultados por página",
				"sLoadingRecords": "<img src='../img/carregando.gif'>",
				"sProcessing": "Processando...",
				"sZeroRecords": "Nenhum registro encontrado",
				"sSearch": "Pesquisar",
				"oPaginate": {
					"sNext": "Próximo",
					"sPrevious": "Anterior",
					"sFirst": "Primeiro",
					"sLast": "Último"
				},
				"oAria": {
					"sSortAscending": ": Ordenar colunas de forma ascendente",
					"sSortDescending": ": Ordenar colunas de forma descendente"
				}
		}
	});
	*/
	/*
	var table = $('#example3').DataTable( {
		"pageLength": 10,
		"language": {
				"sEmptyTable": "Nenhum registro encontrado",
				"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
				"sInfoEmpty": "Mostrando 0 até de 0 registros",
				"sInfoFiltered": "(Filtrados de _MAX_ registros)",
				"sInfoPostFix": "",
				"sInfoThousands": ".",
				"sLengthMenu": "_MENU_ resultados por página",
				"sLoadingRecords": "<img src='../img/carregando.gif'>",
				"sProcessing": "Processando...",
				"sZeroRecords": "Nenhum registro encontrado",
				"sSearch": "Pesquisar",
				"oPaginate": {
					"sNext": "Próximo",
					"sPrevious": "Anterior",
					"sFirst": "Primeiro",
					"sLast": "Último"
				},
				"oAria": {
					"sSortAscending": ": Ordenar colunas de forma ascendente",
					"sSortDescending": ": Ordenar colunas de forma descendente"
				}
		}
	});
	*/
	/*
	var table = $('#example5').DataTable( {
		"pageLength": 10 ,
		"language": {
				"sEmptyTable": "Nenhum registro encontrado",
				"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
				"sInfoEmpty": "Mostrando 0 até de 0 registros",
				"sInfoFiltered": "(Filtrados de _MAX_ registros)",
				"sInfoPostFix": "",
				"sInfoThousands": ".",
				"sLengthMenu": "_MENU_ resultados por página",
				"sLoadingRecords": "<img src='../img/carregando.gif'>",
				"sProcessing": "Processando...",
				"sZeroRecords": "Nenhum registro encontrado",
				"sSearch": "Pesquisar",
				"oPaginate": {
					"sNext": "Próximo",
					"sPrevious": "Anterior",
					"sFirst": "Primeiro",
					"sLast": "Último"
				},
				"oAria": {
					"sSortAscending": ": Ordenar colunas de forma ascendente",
					"sSortDescending": ": Ordenar colunas de forma descendente"
				}
		}

	});*/
});

</script>
<?php
include_once("bottom.php");
?>


