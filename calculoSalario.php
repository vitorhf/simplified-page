<?php
// Seta sem limite de tempo
set_time_limit(0);
// Adiciona libs
include("libs/lib_data.php");
include("libs/lib_geral.php");

// Pegando valores de POST para as variaveis

if (is_array($_POST)) {
   foreach($_POST as $k=>$v) ${$k} = $v;
   if ($salario and !is_numeric($salario)) { // valida o valor do campo salario
      exit();
   }
}

if (!isset($qtdeDependentes)) {
	$qtdeDependentes = 0;
}

$valorBaseDependente = 189.59;

$fgts = 0.08;

$inss = [
	[
		'perc'=>8,
		'value'=>1693.72,
		'fixo'=>0
	],
	[
		'perc'=>9,
		'value'=>2822.90,
		'fixo'=>0
	],
	[
		'perc'=>11,
		'value'=>5645.80,
		'fixo'=>0
	],
	[
		'perc'=>0,
		'value'=>0,
		'fixo'=>621.04
	],
];

$irpf = [
	[
		'value'=>1903.98,
		'perc'=>0,
		'deducao'=>0
	],
	[
		'value'=>2826.65,
		'perc'=>7.5,
		'deducao'=>142.80
	],
	[
		'value'=>3751.05,
		'perc'=>15,
		'deducao'=>354.80
	],
	[
		'value'=>4664.68,
		'perc'=>22.5,
		'deducao'=>636.13
	],
	[
		'value'=>1000000000000,
		'perc'=>27.5,
		'deducao'=>869.36
	],
];

if ($salario) {
	foreach($inss as $i) {
		if ($salario <= $i['value'] and $i['value'] != 0) {
			$cInss = $salario * $i['perc'] / 100;
			break;
		} else if ($i['value'] == 0) {
			$cInss = $i['fixo'];
		}
	}

	$baseCalculoIRPF = $salario - $cInss - $qtdeDependentes * $valorBaseDependente;

	foreach($irpf as $i) {
		if ($baseCalculoIRPF <= $i['value']) {
			$cIrpf = $baseCalculoIRPF * $i['perc'] / 100 - $i['deducao'];
			break;
		}
	}

	$salarioLiquido = $salario - $cInss - $cIrpf;
	$retorno = [
		'salarioLiquido'=>real2($salarioLiquido),
		'INSS'=>real2($cInss),
		'IRRF'=>real2($cIrpf),
		'salarioBruto'=>real2($salario),
		'liquidoAnual'=>real2($salarioLiquido * 12),
		'brutoAnual'=>real2($salario * 12),
		'fgtsMensal'=>real2($salario*$fgts),
		'fgtsAnual'=>real2($salario*$fgts*12)
	];

	echo json_encode($retorno);
}
?>