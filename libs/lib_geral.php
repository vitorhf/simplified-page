<?
function openFile($file, $mode, $input=null) {
   if ($mode == "READ") {
      if (file_exists($file)) {
         $output = file($file);
         return $output; // output file text
      } else {
         return false; // failed.
      }
   } elseif ($mode == "WRITE") {
      $handle = fopen($file, "w");
      if (!fwrite($handle, $input)) {
         return false; // failed.
      } else {
         return true; //success.
      }
   } elseif ($mode == "READ/WRITE") {
      if (file_exists($file) && isset($input)) {
         $handle = fopen($file,"r+");
         $read = fread($handle, filesize($file));
         $data = $read.$input;
         if (!fwrite($handle, $data)) {
            return false; // failed.
         } else {
            return true; // success.
         }
      } else {
         return false; // failed.
      }
   } else {
      return false; // failed.
   }
   fclose($handle);
}

function echos() {
   $args = func_get_args();
   foreach ($args as $arg) echo "[".$arg."]";
   echo "\n\n".br(2);
   flush();
}

function br($quantidade=1) {
   if ($GLOBALS['_SERVER']['DOCUMENT_ROOT']) return str_repeat("<BR>",$quantidade);
   else return str_repeat("\n",$quantidade);
}

function pre($v) {
   echo "<pre>";
   var_dump($v);
   echo "</pre>";
}

/**
**
 * Retorna o valor formatado
 * @param string $valor
 * @return string valor
**/
function real($valor) {
   $valor = (float) $valor;
   setlocale(LC_MONETARY,"pt_BR", "ptb");
   return money_format('%n', $valor);
}


/**
* Formata o valor para o formato monet�rio
* @param float $valor valor a ser formatado
* @return string valor formatado
**/
function real2($valor) {
   if (abs($valor) < 0.01) $valor = 0;
   return(number_format($valor,2,",","."));
}


/**
 * Completa o valor com zeros � esquerda at� o tamanho especificado
 * @param string $campo valor a ser formatado
 * @param integer $tamanho tamanho m�ximo para preenchimento da string
 * @return string valor formatado
**/
function zeros($campo,$tamanho) {
   return str_pad($campo,$tamanho,'0',STR_PAD_LEFT); // 152
}
/**
 * Retira os caracteres ponto (.), tra�o (-), barra (/) e espa�os em branco no �nicio e fim de um valor
 * @param string $numero valor a ser manipulado
 * @return string valor sem formata��o
**/
function tira_formatacao($numero) { // 028
   $numero = str_replace(".","",$numero);
   $numero = str_replace("-","",$numero);
   $numero = str_replace("/","",$numero);
   $numero = trim($numero);
   return $numero;
}
/**
 * Verifica se o cpf � valido e caso seja, formata-o conforme a m�scara 999.999.999-99
 * @param string $cpf valor do cpf
 * @return bool true se v�lido e false se inv�lido
**/
function valida_cpf($cpf) { // 028
   $numero = tira_formatacao($cpf);
   $tamnum = strlen($numero);
   if ($tamnum != 11 or !is_numeric($numero)) return false;
   for ($i=0;$i<=9;++$i) {
      if ($numero == str_repeat($i,11)) {
         $cpf = formata_string($cpf,"999.999.999-99");
         return false;
      }
   }
   $dv = 0;
   for ($i=0;$i<=8;++$i) {
      $dv += substr($numero,$i,1)*(10-$i);
   }
   $dv = ($dv*10)%11;
   $dv1 = right($dv,1);
   $dv_informado = right($numero,2);
   $numero = left($numero,9).$dv1;
   $dv = 0;
   for ($i=0;$i<=9;++$i) {
      $dv += substr($numero,$i,1)*(11-$i);
   }
   $dv = ($dv*10)%11;
   $dv = right($dv,1);
   $dv = $dv1.$dv;
   $cpf = formata_string($cpf,"999.999.999-99");
   if ($dv_informado == $dv) return true;
   else return false;
}
/**
 * Verifica se o cnpj � valido e caso seja, formata-o conforme a m�scara 99.999.999/9999-99
 * @param string $cnpj valor do cnpj
 * @return bool true se v�lido e false se inv�lido
**/
function valida_cnpj($cnpj) { // 028
   $numero = tira_formatacao($cnpj);
   $tamnum = strlen($numero);
   if ($tamnum != 14 or !is_numeric($numero)) return false;
   $dv = 0;
   for ($i=0;$i<=11;++$i) {
      $dv += substr($numero,$i,1)*(($i<4)?(5-$i):(13-$i));
   }
   $dv = ($dv*10)%11;;
   $dv1 = right($dv,1);
   $dv_informado = right($numero,2);
   $numero = left($numero,12).$dv1;
   $dv = 0;
   for ($i=0;$i<=12;++$i) {
      $dv += substr($numero,$i,1)*(($i<5)?(6-$i):(14-$i));
   }
   $dv = ($dv*10)%11;
   $dv = right($dv,1);
   $dv = $dv1.$dv;
   $cnpj = formata_string($cnpj,"99.999.999/9999-99");
   if ($dv_informado == $dv) return true;
   else return false;
}
/**
 * Formata uma string de acordo com a m�scara especificada
 * @global bool fica true caso a string n�o satisfa�a a m�scara para formata��o (quando o tamanho sem formata��o for diferente do definido na m�scara)
 * @param string $string string a ser formatada
 * @param string $mascara m�scara para formata��o
 * @return string string formatada
**/
function formata_string($string,$mascara) { // 028 // 051
   global $formata_string_erro; // 161
   $formata_string_erro = false; // 161
   $mascara = trim($mascara);
   if (strlen(str_replace('9','',$mascara))==0) return zeros($string,strlen($mascara)); // 170
   $fim = strlen($mascara);
   for ($i=0;$i<$fim;++$i) {
      $caracter = substr($mascara,$i,1);
      if (in_array($caracter,array('9','A','a','N','n','X'))) ++$tamanho;
      else $string = str_replace($caracter,'',$string);
   }
   if (!$string) return $string;
   if (strlen($string)!=$tamanho) $formata_string_erro = true; // 161
   $j = 0;
   for ($i=0;$i<$fim;++$i) {
      $caracter = substr($mascara,$i,1);
      if      ($caracter == '9') $nova_string .= $string[$j++];
      else if ($caracter == 'A') $nova_string .= strtoupper($string[$j++]);
      else if ($caracter == 'a') $nova_string .= strtolower($string[$j++]);
      else if ($caracter == 'N') $nova_string .= strtoupper($string[$j++]);
      else if ($caracter == 'n') $nova_string .= strtolower($string[$j++]);
      else if ($caracter == 'X') $nova_string .= $string[$j++];
      else $nova_string .= $caracter;
   }
   return $nova_string;
}
/**
 * Valida uma string de acordo com a m�scara especificada, por meio de express�o regular
 * @param string $string string a ser validada
 * @param string $mascara m�scara para valida��o
 * @return bool true se v�lido e false se inv�lido
**/
function valida_string($string,$mascara) {  // 051
   $mascara = trim($mascara);
   $fim = strlen($mascara);
   for ($i=0;$i<$fim;++$i) {
      $caracter = substr($mascara,$i,1);
      if      ($caracter == '9') $er .= "[0-9]";
      else if ($caracter == 'A') $er .= "[A-Z]";
      else if ($caracter == 'a') $er .= "[a-z]";
      else if ($caracter == 'N') $er .= "[A-Z0-9]";
      else if ($caracter == 'n') $er .= "[a-z0-9]";
      else if ($caracter == 'X') $er .= ".";
      else                       $er .= "[".$caracter."]";
   }
   return (ereg($er,$string));
}

/**
 * Exibe mensagem de alerta em javascript
 * @param string $alerta mensagem a ser exibida
 * @param bool $aguarde_on se true, liga o aguarde do sistema ap�s exibir a mensagem
 * @return bool
**/
function alerta($alerta,$aguarde_on=0) { // 117
   $alerta = str_replace("\"","'",$alerta); // 063
   $alerta = str_replace(chr(10),'\n',$alerta); // 063
   // 205
   if (!$GLOBALS['_SERVER']['DOCUMENT_ROOT']) echo $alerta;
   else {
      $script = "<SCRIPT>\n"
              . "    var script_carregado;\n" // 052 // 064
              . "    if (script_carregado) aguarde_off();\n" // 052 // 064
              . "    alert(\"".$alerta."\");\n"
              . ($aguarde_on?"    aguarde();\n":"") // 117
              . "</SCRIPT>\n";
      echo $script;
      flush();
   }
   return true;
}
/**
 * Valida o endere�o de e-mail
 * @param string $valor e-mail
 * @return mixed e-mail se v�lido e false se inv�lido
**/
function valida_email ($valor) { // 206
   $regex = "(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"; 
   //$expressao = "^[_\.0-9a-z-]+@([0-9a-z]+\.)+[a-z]{2,3}$";
   return (!$valor OR !preg_match($regex, $valor))? FALSE : $valor;
}
/**
 * Executa um comando em javascript
 * @param string $comando comando para execu��o
**/
function js($comando) { // 049
   $js  = "\n<SCRIPT>\n"; // 215
   $js .= "$comando";
   $js .= "</SCRIPT>\n";
   echo $js;
}
/**
 * Retira caracteres acentuados de textos
 * @param string $texto texto
 * @return string texto sem acentua��o
**/
function tira_acentos($texto) { // 073
   $a = '���������������������������������������������������';
   $b = 'aaaaaAAAAAeeeeEEEEiiiiIIIIoooooOOOOOuuuuUUUUcCnNyyY';
   return strtr($texto,$a,$b);
}


/**
 * Escreve o valor por extenso
 * @param mixed valor
 * @param boolean monetario
 * @return string valor por extenso
**/
function extenso_real($valor,$monetario=true) { // 302
   return extenso_numero($valor,$m='M',$monetario); // 303
}
/**
 * Escreve o valor por extenso
 * @param mixed valor
 * @return string valor por extenso
**/
function extenso_numero($valor,$m='M',$monetario=false) { // 240 // 302 // 303
   if (!trim($m)) $m= 'M';
   if (strpos($valor,',')!==false) {
      $valor = str_replace(',','@',$valor);
      $valor = str_replace('.','',$valor);
      $valor = str_replace('@','.',$valor);
   }
   $valor = number_format($valor,2,',','.');
   list($v_inteiro,$v_decimal) = explode(',',$valor);
   // decimais
   if ($monetario) {
      $v_decimal = (int)($v_decimal);
      $decimal = extenso_dezena($v_decimal,$m);
      $decimal.= ($v_decimal?' centavo'.($v_decimal>1?'s':''):'');
   }
   // inteiros
   // centenas - de 100 a 900
   if ($m=='F') $centenas = array(1=>'cento','duzentas','trezentas','quatrocentas','quinhentas','seiscentas','setecentas','oitocentas','novecentas'); // feminino // 303
   else $centenas = array(1=>'cento','duzentos','trezentos','quatrocentos','quinhentos','seiscentos','setecentos','oitocentos','novecentos'); // masculino // 303
   // demais nomeclaturas
   $demais['s'] = array($monetario?'real':'','mil','milh�o','bilh�o','trilh�o'); // singular
   $demais['p'] = array($monetario?'reais':'','mil','milh�es','bilh�es','trilh�es'); // plural
   $v_inteiro = explode('.',$v_inteiro);
   $i=count($v_inteiro)-1;
   $cont=0;
   foreach ($v_inteiro as $v) {
      $v = zeros($v,3);
      $c = (int)(substr($v,0,1));
      $d = (int)(substr($v,1,2));
      $com_e = true;
      if (!$c) {
         if ($d) $com_e = false;
         else {
            $cont++;
            if (!$i and $inteiro) {
               if ($cont>=2) $inteiro.= ' de';
               if ($monetario) $inteiro.= ' reais';
            }
            $i--;
            continue;
         }
      } else if ($c) {
         $cont=0;
         $inteiro.= ($inteiro?' ':'');
         if ($d>0) $inteiro.= $centenas[$c];
         else {
            if ($c>1) $inteiro.= $centenas[$c];
            else $inteiro.= 'cem'; // exce��o para 100
            $com_e = false;
         }
      }
      $inteiro.= (($inteiro and $com_e)?' e':'');
      $inteiro.= ' '.extenso_dezena($d,$m);
      $inteiro.= ' '.$demais[($v>1?'p':'s')][$i];
      $i--;
   }
   return trim($inteiro.(($inteiro and $decimal)?' e ':'').($monetario?$decimal:''));
}
/**
 * Escreve o valor por extenso somente dezenas (de 1 a 99)
 * @param mixed valor
 * @return string valor por extenso
**/
function extenso_dezena($v,$s) { // 240 // 302
   // unidades - de 1 - 9
   $sexo['M'] = array(1=>'um','dois','tr�s','quatro','cinco','seis','sete','oito','nove');
   $sexo['F'] = array(1=>'uma','duas','tr�s','quatro','cinco','seis','sete','oito','nove');
   $unidades = $sexo[$s];
   // dezenas_excessao - de 10 a 19
   $dezenas_excessao = array(10=>'dez','onze','doze','treze','quatorze','quinze','dezesseis','dezessete','dezoito','dezenove');
   // dezenas - de 20 a 90
   $dezenas = array(2=>'vinte','trinta','quarenta','cinquenta','sessenta','setenta','oitenta','noventa');
   //
   if (!$v) return '';
   else {
      if ($v<=9) $decimal = $unidades[$v];
      else if ($v<=19) $decimal = $dezenas_excessao[$v];
      else {
         $d = (int)(substr($v,0,1));
         $u = (int)(substr($v,1,1));
         $decimal = $dezenas[$d];
         if ($u) $decimal.= ' e '.$unidades[$u];
      }
   }
   return $decimal;
}
?>
