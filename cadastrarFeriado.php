<?php

include_once("top.php");
include_once("header.php");

$arquivo = "arq/feriados.txt";

// Pegando valores de POST para as variaveis
if (is_array($_POST)) {
   foreach($_POST as $k=>$v) ${$k} = $v;
}
if(is_writable($arquivo)) { // verifica se o arquivo pode ser escrito
   if ($feriados) {
      $testes = explode("\n",$feriados); // adiciona em array
      $testados = array();
      if (is_array($testes)) {
         foreach($testes as $k=>$data) {
            $data = trim($data);
            $dtok = dtok($data); // verifica se a data é valida
            if ($data) {
               if (!$dtok) $erro = 'N&atilde;o foi poss&iacute;vel gravar. Data inv&aacute;lida ('.$data.').';
               else $testados[] = $data; // ignora todas as linhas do textarea vazia
            }
         }
      }
      $testados = implode("\n",$testados); // transforma para texto para ser adicionado no arquivo txt
      if (!$erro) {
         $file = openFile($arquivo,'WRITE',$testados); // grava no arquivo txt
         if (!$file) $erro = 'Erro ao tentar atualizar a lita de feriados. Entre em contato com o administrador (vitorhf@gmail.com).';
      }
   }
}

if (file_exists($arquivo)) { // verifica se o arquivo existe
   if (is_readable($arquivo)) $file = openFile($arquivo,'READ'); // verifica se pode ler o arquivo
   else $erro = 'Sem permiss&atilde;o para ler o arquivo de feriados. Entre em contato com o administrador (vitorhf@gmail.com).';
}

?>
      <!-- Full Width Column -->
      <div class="content-wrapper">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              Cadastro de Feriados
            </h1>
          </section>

          <!-- Main content -->
          <section class="content">
			<form role="form" NAME="form1" method="post">
            <div class="row">
               <div class="col-md-12">
			      <div class="box-body">
					<div class="form-group">
                      <label for="salario" class="col-sm-2 control-label">Datas:</label>
                      <div class="col-sm-4">
						<textarea class="form-control" id="feriados" name="feriados" ROWS="20" cols="15">
<?
if (is_array($file)) {
   foreach($file as $v) echo $v; // adiciona o conteudo do arquivo no textarea
}
?>						
						</textarea>
                      </div>
                    </div>
				  </div>
				</div>
			   </div>
			  <div class="row">
                 <div class="col-md-6">
			       <div class="box-body">
					<div class="form-group">
                      <input type="submit" class="btn btn-primary pull-right " id="btnGravar" name="btnGravar" value="Gravar">
					  <button type="button" class="btn btn-primary" id="btnVoltar" name="btnVoltar">Voltar</button>
                    </div>
                  </div><!-- /.box-body -->
			   </div>
			  </div>
			  </form>
	      </section>
        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->
<?php
include_once("footer.php");
include_once("script_footer.php");
?>
<script>
$(document).ready(function() {

	$("#btnVoltar").click(function() {
		window.location = "ferias.php";
	});
});
</script>
<?php
include_once("bottom.php");
?>      


