<?php
include_once("top.php");
include_once("header.php");

$arqs = ["arq/email_calendar_cit.txt","arq/email_calendar_pessoal.txt"];

if (!empty($_POST)) {
    foreach($_POST as $k=>$v) {
        ${$k} = $v;
    }
}

if ($oper == 'emailCIT') {
  if (trim($enderecoGmail1) != '') {
    $enderecoGmail1 = str_replace("\n","<BR>",$enderecoGmail1);
    $fp = fopen($arqs[0],"a");
    fwrite($fp, $enderecoGmail1);
    fclose($fp);
  }
} else if ($oper == 'emailPessoal') {
  if (trim($enderecoGmail2) != '') {
    $enderecoGmail2 = str_replace("\n","<BR>",$enderecoGmail2);
    $fp = fopen($arqs[1],"a");
    fwrite($fp, $enderecoGmail2);
    fclose($fp);
  }
}

foreach($arqs as $k=>$arq) {
  if (is_file($arq)) {
    $d = file($arq);
    foreach($d as $email) {
      ${'enderecoGmail'.($k+1)} = $email;
    }
  }
}

?>
      <!-- Full Width Column -->
      <div class="content-wrapper">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              Google Calendar
            </h1>
          </section>

          <!-- Main content -->
          <section class="content">
            <div class="row">
              <div class="col-md-12">
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">CI&T</a></li>
                    <li><a href="#tab_2" data-toggle="tab">Pessoal</a></li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                      <?php
                        if (empty($enderecoGmail1)) {
                          ?>
                          <form name="cit" method="POST" action="calendar.php">
                            E-mail CI&T:<input type="text" class="form-control" id="enderecoGmail1" name="enderecoGmail1">
                            <input type="hidden" id="oper" name="oper" value="emailCIT">
                            <button type="submit" class="btn btn-primary">Salvar</button>
                          </form>
                          <?php
                        } else {
                          echo '<iframe src="https://calendar.google.com/calendar/embed?src='.$enderecoGmail1.'&ctz=America%2FSao_Paulo" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>';
                        }
                      ?>

                    </div>
                    <div class="tab-pane" id="tab_2">
                    <?php
                        if (empty($enderecoGmail2)) {
                          ?>
                          <form name="pessoal" method="POST" action="calendar.php">
                            E-mail Pessoal: <input type="text" class="form-control" id="enderecoGmail2" name="enderecoGmail2">
                            <input type="hidden" id="oper" name="oper" value="emailPessoal">
                            <button type="submit" class="btn btn-primary">Salvar</button>
                          </form>
                          <?php
                        } else {
                          echo '<iframe src="https://calendar.google.com/calendar/embed?src='.$enderecoGmail2.'%40gmail.com&ctz=America%2FSao_Paulo" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>';
                        }
                      ?>
                    </div>
                </div>
              </div>
            </div>
		    </div>
	      </section>
        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->
<?php
include_once("footer.php");
include_once("script_footer.php");
include_once("bottom.php");
?>


