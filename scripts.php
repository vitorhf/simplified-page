<?php
include_once("top.php");
include_once("header.php");

foreach($_GET as $k=>$v) {
	${$k} = $v;
}
foreach($_POST as $k=>$v) {
	${$k} = $v;
}
date_default_timezone_set('America/Sao_Paulo');
$date = date('d/m/Y');
$pasta = "arq/scripts";

$table = '';

foreach (glob($pasta."/*.sql") as $filename) {
	$assunto = explode('/',$filename);
	$qtde = count($assunto);
	$assunto = explode('.',$assunto[$qtde-1]);
	$assunto = $assunto[0];
	$botao = '<button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal" data-reg="'.$assunto.'"><i class="fa fa-eye"></i></button>';
	$table .= '<tr>';
	$table .= '<td>'.str_replace('_',' ',$assunto).'</td>';
	$table .= '<td>'.$botao.'</td>';
	$table .= '</tr>';
}

?>
      <!-- Full Width Column -->
      <div class="content-wrapper">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              SCRIPTS
            </h1>
          </section>
			<div class="row">
				<div class="col-md-12" id="divResult">
			    <div class="box">
					<div class="box-body">
						<table id="example2" class="table table-bordered table-hover stripe" cellspacing="0" width="100%">
						<thead>
						  <tr>
							<th><B>ASSUNTO</B></th>
							<th><B>VISUALIZAR</B></th>
						  </tr>
						</thead>
						<tbody>
						<?=$table?>
						</tbody>
						</table>
					</div>
				</div>
			   </div>
			</div>
	      </section>
        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->
	  <!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Querys</h4>
			  </div>
			  <div class="modal-body">
				...
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>
		  </div>
		</div>
<?php
include_once("footer.php");
include_once("script_footer.php");
?>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function() {


	var table = $('#example2').DataTable( {
		"pageLength": 100,
		"language": {
				"sEmptyTable": "Nenhum registro encontrado",
				"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
				"sInfoEmpty": "Mostrando 0 até de 0 registros",
				"sInfoFiltered": "(Filtrados de _MAX_ registros)",
				"sInfoPostFix": "",
				"sInfoThousands": ".",
				"sLengthMenu": "_MENU_ resultados por página",
				"sLoadingRecords": "<img src='../img/carregando.gif'>",
				"sProcessing": "Processando...",
				"sZeroRecords": "Nenhum registro encontrado",
				"sSearch": "Pesquisar",
				"oPaginate": {
					"sNext": "Próximo",
					"sPrevious": "Anterior",
					"sFirst": "Primeiro",
					"sLast": "Último"
				},
				"oAria": {
					"sSortAscending": ": Ordenar colunas de forma ascendente",
					"sSortDescending": ": Ordenar colunas de forma descendente"
				}
		}
	});
	
	$('#myModal').on('show.bs.modal', function (event) {
	  var button = $(event.relatedTarget); // Button that triggered the modal
	  var num = button.data('reg'); // Extract info from data-* attributes
	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	  var modal = $(this);
	  modal.find('.modal-title').text(num);
	  $.ajax({
			url: 'buscaScript.php',
			type: 'POST',
			data: {reg:num},
			success: function(r){
				console.log('retorno');
				console.log(r);
				console.log('pos');
				modal.find('.modal-body').text(r);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				//alert('Erro ao buscar informações! 1');
				console.log(textStatus);
				console.log(XMLHttpRequest);
				console.log(errorThrown);
			}
		});
	  
	})
	
	
});

function atualizar() {
		$.ajax({
			url: 'https://correios.postmon.com.br/webservice/buscaEventos/?objetos=<?=$numeros?>',
			method: 'GET',
			dataType: "json",
			success: function(r){
				console.log(r.objeto);
				var objetos = r.objeto;
				var numRastreio = '';
				var rota = '';
				var dataHora = '';
				var ultimoReg = 0;
				for (var i = 0, len = objetos.length; i < len; i++) {
					//console.log(objetos[i]);
					$.ajax({
						url: 'gravaRastreio.php',
						type: 'POST',
						dataType: "json",
						data: objetos[i],
						cache: false,
						success: function(r){
							console.log(r);
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							//alert('Erro ao buscar informações! 1');
							console.log(textStatus);
							console.log(XMLHttpRequest);
							console.log(errorThrown);
						}
					});
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				alert('Erro ao buscar informações 2!');
				console.log(textStatus);
				console.log(XMLHttpRequest);
				console.log(errorThrown);
			}
		});

		
	}

</script>
<?php
include_once("bottom.php");
?>      


