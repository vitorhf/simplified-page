<?php
include_once("top.php");
include_once("header.php");

foreach($_GET as $k=>$v) {
	${$k} = $v;
}
foreach($_POST as $k=>$v) {
	${$k} = $v;
}


?>
      <!-- Full Width Column -->
      <div class="content-wrapper">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              Inverter
            </h1>
          </section>

          <!-- Main content -->
          <section class="content">
            <div class="row">
				<form action="inveter.php?oper=salvar" method="POST">
					<div class="box-body">
						<div class="form-group">
							<label for="descricao" class="col-sm-2 control-label" style="top:7px">Texto: </label>
							<div class="col-sm-4">
								<textarea class="form-control" id="texto" name="texto"></textarea>
							</div>
							<div class="col-sm-2">
								<button type="button" class="btn btn-primary" onclick="inverter()">Inverter</button>
							</div>
						</div>
					</div><!-- /.box-body -->
				</form>
		    </div>
			<div class="row">
				<div class="col-md-12" id="divResult">
			    <div class="box">
					<div class="box-header">
						<h3 class="box-title">Resultado</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div id="result"></div>
						<button type="button" class="btn btn-primary" onclick="copy()" id="copiar" style="display:none">Copiar</button>
						
					</div>
				</div>
			   </div>
			</div>
	      </section>
        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

<?php
include_once("footer.php");
include_once("script_footer.php");
?>
<script>
	function inverter() {
		var t = document.getElementById("texto").value;
		document.getElementById("result").innerHTML = t.split('').reverse().join('');
		document.getElementById("copiar").style.display = 'block';
	}
	
	function copy() {
		var copyText = document.getElementById("result").innerHTML;
		copyText.select();
		document.execCommand("copy");alert("Copied the text: " + copyText.value);
	}
</script>
<?php
include_once("bottom.php");
?>      


