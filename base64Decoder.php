<?php
include_once("top.php");
include_once("header.php");

foreach($_GET as $k=>$v) {
	${$k} = $v;
}
foreach($_POST as $k=>$v) {
	${$k} = $v;
}

if ($oper == 'decodificar') {
	if (!empty($texto)) {
		$result = base64_decode($texto);
	}
}


?>
      <!-- Full Width Column -->
      <div class="content-wrapper">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              Base64 Decode
            </h1>
          </section>

          <!-- Main content -->
          <section class="content">
            <div class="row">
				<form action="base64Decoder.php?oper=decodificar" method="POST">
					<div class="box-body">
						<div class="form-group">
							<label for="descricao" class="col-sm-2 control-label" style="top:7px">Texto: </label>
							<div class="col-sm-4">
								<textarea class="form-control" id="texto" name="texto"></textarea>
							</div>
							<div class="col-sm-2">
								<button type="submmit" class="btn btn-primary">Decode</button>
							</div>
						</div>
					</div><!-- /.box-body -->
				</form>
		    </div>
			<div class="row">
				<div class="col-md-12" id="divResult">
			    <div class="box">
					<div class="box-header">
						<h3 class="box-title">Resultado</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div id="result"></div>
						<button type="button" class="btn btn-primary" onclick="copy()" id="copiar" style="display:none">Copiar</button>
						
					</div>
				</div>
			   </div>
			</div>
	      </section>
        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

<?php
include_once("footer.php");
include_once("script_footer.php");
?>
<script>
	
	function copy() {
		var copyText = document.getElementById("result").innerHTML;
		copyText.select();
		document.execCommand("copy");alert("Copied the text: " + copyText.value);
	}
	<?php 
	if (!empty($result)) {
	echo "
		document.getElementById('result').innerHTML = '".$result."';
		
		";
	}
	?>
</script>
<?php
include_once("bottom.php");
?>      


