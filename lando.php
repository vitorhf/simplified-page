<?php
include_once("top.php");
include_once("header.php");
$d = [
	['lando info','Lista informações sobre o app'],
	['lando start','Inicia app'],
	['lando restart','Reinicia app'],
	['lando stop','Paraliza app'],
	["lando db-import dumpfile.zip",'Importa o dump para a base local'],
	['lando db-export ou platform db:dump','Exporta a base de dados local'],
	['lando composer', 'Roda comandos composer'],
	['lando drupal','Roda comandos drupal'],
	['lando drush','Roda comandos drusg'],
	['lando mysql','Roda comandos mysql'],
	['lando php','Roda comandos php'],
	['lando drush status','Checa o status do site com drush'],
	['lando drush fra -y','Reverte todas as features'],
	['lando drush fr machineNameFeature','Reverte uma feature especifica'],
	['lando drush cr','Limpa cache'],
	['lando drush ard --destination=path-to-store-the-file','Gera um backup dos arquivos e banco'],
	['lando devel','Ativa features'],
	['lando update','Atualiza o site (composer update e drupal updb)'],
	['lando destroy','Destroi os containers e ferramentas para o app'],
	['lando rebuild','Refaz todos os containers e constroi os processos.'],
	['drush en machineName','Instala uma feature.'],
];

//Lê o conteúdo do arquivo aberto.
foreach($d as $v) {
	$table .= '<tr>';
	$table .= '<td>'.$v[0].'</td>';
	$table .= '<td>'.$v[1].'</td>';
	$table .= '</tr>';
}

?>
<!-- Full Width Column -->
      <div class="content-wrapper">
        <div class="container">
          <!-- Main content -->
          <section class="content">
			<div class="row">
				<div class="col-md-12" id="divResult">
					<div class="box">
						<section class="content-header">
							<h1>
							LANDO<button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#modal-todo"><i class="fa fa-plus" aria-hidden="true"></i></button>
							</h1>
						</section>
						<div class="box-body">
							<table id="example2" class="display cell-border compact stripe hover table table-striped table-bordered" cellspacing="0" width="100%">
							<thead>
							  <tr>
								<th><B>COMANDO</B></th>
								<th><B>DESCRIÇÃO</B></th>
							  </tr>
							</thead>
							<tbody>
							<?=$table?>
							</tbody>
							</table>
						</div>
					</div>
			   </div>
			 </div>
	      </section>
        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->
<?php
include_once("footer.php");
include_once("script_footer.php");
?>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function() {


	var table = $('#example2').DataTable( {
		"pageLength": 50,
		"language": {
				"sEmptyTable": "Nenhum registro encontrado",
				"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
				"sInfoEmpty": "Mostrando 0 até de 0 registros",
				"sInfoFiltered": "(Filtrados de _MAX_ registros)",
				"sInfoPostFix": "",
				"sInfoThousands": ".",
				"sLengthMenu": "_MENU_ resultados por página",
				"sLoadingRecords": "<img src='../img/carregando.gif'>",
				"sProcessing": "Processando...",
				"sZeroRecords": "Nenhum registro encontrado",
				"sSearch": "Pesquisar",
				"oPaginate": {
					"sNext": "Próximo",
					"sPrevious": "Anterior",
					"sFirst": "Primeiro",
					"sLast": "Último"
				},
				"oAria": {
					"sSortAscending": ": Ordenar colunas de forma ascendente",
					"sSortDescending": ": Ordenar colunas de forma descendente"
				}
		}
	});

});

</script>
<?php
include_once("bottom.php");
?>
