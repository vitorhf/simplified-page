<footer class="main-footer">
	<div class="container">
	  <div class="pull-right hidden-xs">
		<b>Version</b> 1.2.3
	  </div> 
	  <strong>Copyright &copy; 2018 <a href="http://almsaeedstudio.com">Vitor Hallais</a>.</strong> All rights reserved.
	</div><!-- /.container -->
  </footer>
</div><!-- ./wrapper -->