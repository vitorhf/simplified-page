<?php
// Seta sem limite de tempo
set_time_limit(0);
// Adiciona libs
include("libs/lib_data.php");
include("libs/lib_geral.php");
$arquivo = "arq/feriados.txt";

// Pegando valores de POST para as variaveis
if (is_array($_POST)) {
   foreach($_POST as $k=>$v) ${$k} = $v;
   if ($dias and !is_numeric($dias)) { // valida o valor do campo dias
      exit();
   }
}
if ($dias) {
   if (file_exists($arquivo)) {
      $file = openFile($arquivo,'READ');
      $marcacoes = $feriados_envolvidos_ant = $feriados_envolvidos_dps = $maiores = array();
	  $indice = 1;
      foreach ($file as $k=>$v) {
         ############################################################
         ##### VERIFICA A QUANTIDADE DE DIAS ANTES DOS FERIADOS #####
         ############################################################
         unset($inicio,$final,$data_int,$qtde_dias,$data_oficial_saida); // limpa as variaveis para as proximas datas
         if (in_array($v,$feriados_envolvidos_ant)) continue; // verifica se o feriado ja foi envolvida em alguma conta anterior
         $qtde_dias = $dias + 1; // soma de quantidade de dias informadas + 1 do feriado
         // verifica os dias antes do feriado
         $data_int = $final = dt($v); // data do feriado em numero
         $tenta_fds_ant_feriado = dts($data_int - 1); // verifica o dia da semana de 1 dia antes do feriado
         $tenta_fds_dps_feriado = dts($data_int + 1); // verifica o dia da semana de 1 dia depois do feriado
         // soma os dias de sabado e domingo depois do periodo
         if (diasemana($tenta_fds_dps_feriado) == 'Domingo') {
            $qtde_dias += 1;
            $final += 1;
         } else if (diasemana($tenta_fds_dps_feriado) == 'Sabado') {
            $qtde_dias += 2;
            $final += 2;
         }
         // soma os dias de sabado e domingo antes do periodo
         if (diasemana($tenta_fds_ant_feriado) == 'Domingo') {
            $qtde_dias += 2;
            $inicio -= 2;
         } else if (diasemana($tenta_fds_ant_feriado) == 'Sabado') {
            $qtde_dias += 1;
            $inicio -= 1;
         }
         $inicio = $data_int + $inicio - $dias; // data de inicio das ferias
         $tenta_fds_ant = dts($inicio - 1); // verifica o dia da semana de 1 dia antes da data de inicio das ferias
         // soma os dias de sabado e domingo antes do inicio do periodo
         if (diasemana($tenta_fds_ant) == 'Domingo') {
            $qtde_dias += 2;
            $inicio -= 2;
            $data_oficial_saida = 2;
         } else if (diasemana($tenta_fds_ant) == 'Sabado') {
            $qtde_dias += 1;
            $inicio -= 1;
            $data_oficial_saida = 2;
         }
         if ($data_oficial_saida) $data_oficial_saida += $inicio;
         else $data_oficial_saida = $inicio;
         // verifica os outros feriados para verificar se existe feriados seguidos
         for ($i=1;$i<=10;$i++) {
            $prox_feriado = dt($file[$k+$i]);
            if (!$prox_feriado) break;
            else {
               if ($prox_feriado != ($final+1)) break;
               else {
                  $final = $prox_feriado;
                  $qtde_dias++;
                  if (diasemana(dts($final))=='Sexta') {
                     $qtde_dias += 2;
                     $final += 2;
                  }
                  $feriados_envolvidos_ant[] = $file[$k+$i];
               }
            }
         }
         // verifica os outros feriados para verificar se existe feriados anteriores
         $j = 0;
         for ($i=10;$i>=1;$i--) {
            $j++;
            $ant_feriado = dt($file[$k-$j]);
            if (!$ant_feriado) break;
            else {
               if ($ant_feriado != ($inicio-1)) break;
               else {
                  $inicio = $ant_feriado;
                  $qtde_dias++;
                  if (diasemana(dts($inicio))=='Segunda') {
                     $qtde_dias += 2;
                     $inicio -= 2;
                  }
                  $feriados_envolvidos_ant[] = $file[$k-$j];
               }
            }
         }
         $dt_inicio=dts($inicio);
         $dt_final=dts($final);
         $marcacoes[] = array( 'dt_inicio'=>$dt_inicio
                             , 'dt_ini_semana'=>diasemana($dt_inicio)
                             , 'dt_final'=>$dt_final
                             , 'dt_fin_semana'=>diasemana($dt_final)
                             , 'qtde_dias'=>$qtde_dias
                             , 'dt_saida_oficial'=>dts($data_oficial_saida)
                             , 'dt_volta_oficial'=>dts($final+1)
							 , 'destaque'=>''
							 , 'indice'=>$indice++
                             )
         ;
         // destaca os periodos com mais dias
         if (!$maiores1) $maiores1 = $qtde_dias;
         else if (!$maiores2) $maiores2 = $qtde_dias;
         else if (!$maiores3) $maiores3 = $qtde_dias;
         if ($maiores1 <= $maiores2 and $maiores1 <= $maiores3) $menor = 1;
         else if ($maiores2 <= $maiores1 and $maiores2 <= $maiores3) $menor = 2;
         else if ($maiores3 <= $maiores1 and $maiores3 <= $maiores2) $menor = 3;
         if ($qtde_dias > ${'maiores'.$menor}) ${'maiores'.$menor} = $qtde_dias;


         ###########################################################
         ##### VERIFICA A QUANTIDADE DE DIAS APÓS DOS FERIADOS #####
         ###########################################################
         unset($inicio,$final,$data_int,$qtde_dias,$data_oficial_saida); // limpa as variaveis para as proximas datas
         if (in_array($v,$feriados_envolvidos_dps)) continue; // verifica se o feriado ja foi envolvida em alguma conta anterior
         $qtde_dias = $dias + 1; // soma de quantidade de dias informadas + 1 do feriado
         // verifica os dias antes do feriado
         $data_int = $inicio = dt($v); // data do feriado em numero
         $tenta_fds_ant_feriado = dts($data_int - 1); // verifica o dia da semana de 1 dia antes do feriado
         $tenta_fds_dps_feriado = dts($data_int + 1); // verifica o dia da semana de 1 dia depois do feriado
         // soma os dias de sabado e domingo depois do periodo
         if (diasemana($tenta_fds_dps_feriado) == 'Domingo') {
            $qtde_dias += 1;
            $data_oficial_saida = 2;
            $final += 1;
         } else if (diasemana($tenta_fds_dps_feriado) == 'Sabado') {
            $qtde_dias += 2;
            $data_oficial_saida = 2;
            $final += 2;
         }
         // soma os dias de sabado e domingo antes do periodo
         if (diasemana($tenta_fds_ant_feriado) == 'Domingo') {
            $qtde_dias += 2;
         } else if (diasemana($tenta_fds_ant_feriado) == 'Sabado') {
            $qtde_dias += 1;
         }
         $final = $data_int + $final + $dias; // data de inicio das ferias
         $tenta_fds_dps = dts($final + 1); // verifica o dia da semana de 1 dia depois da data de inicio das ferias
         // soma os dias de sabado e domingo antes do inicio do periodo
         if (diasemana($tenta_fds_dps) == 'Sabado') {
            $qtde_dias += 2;
            $final += 2;
         } else if (diasemana($tenta_fds_ant) == 'Domingo') {
            $qtde_dias += 1;
            $final += 1;
         }
         if ($data_oficial_saida) $data_oficial_saida += $inicio + 1;
         else $data_oficial_saida = $inicio + 1;
         // verifica os outros feriados para verificar se existe feriados seguidos
         for ($i=1;$i<=10;$i++) {
            $prox_feriado = dt($file[$k+$i]);
            if (!$prox_feriado) break;
            else {
               if ($prox_feriado != ($final+1)) break;
               else {
                  $final = $prox_feriado;
                  $qtde_dias++;
                  if (diasemana(dts($final))=='Sexta') {
                     $qtde_dias += 2;
                     $final += 2;
                  }
                  $feriados_envolvidos_ant[] = $file[$k+$i];
               }
            }
         }
         // verifica os outros feriados para verificar se existe feriados anteriores
         $j = 0;
         for ($i=10;$i>=1;$i--) {
            $j++;
            $ant_feriado = dt($file[$k-$j]);
            if (!$ant_feriado) break;
            else {
               if ($ant_feriado != ($inicio-1)) break;
               else {
                  $inicio = $ant_feriado;
                  $qtde_dias++;
                  if (diasemana(dts($inicio))=='Segunda') {
                     $qtde_dias += 2;
                     $inicio -= 2;
                  }
                  $feriados_envolvidos_ant[] = $file[$k-$j];
               }
            }
         } 
         $dt_inicio=dts($inicio);
         $dt_final=dts($final);
         $marcacoes[] = array( 'dt_inicio'=>$dt_inicio
                             , 'dt_ini_semana'=>diasemana($dt_inicio)
                             , 'dt_final'=>$dt_final
                             , 'dt_fin_semana'=>diasemana($dt_final)
                             , 'qtde_dias'=>$qtde_dias
                             , 'dt_saida_oficial'=>dts($data_oficial_saida)
                             , 'dt_volta_oficial'=>dts($final+1)
							 , 'destaque'=>''
							 , 'indice'=>$indice++
                             )
         ;
         // destaca os periodos com mais dias
         if (!$maiores1) $maiores1 = $qtde_dias;
         else if (!$maiores2) $maiores2 = $qtde_dias;
         else if (!$maiores3) $maiores3 = $qtde_dias;
         if ($maiores1 <= $maiores2 and $maiores1 <= $maiores3) $menor = 1;
         else if ($maiores2 <= $maiores1 and $maiores2 <= $maiores3) $menor = 2;
         else if ($maiores3 <= $maiores1 and $maiores3 <= $maiores2) $menor = 3;
         if ($qtde_dias > ${'maiores'.$menor}) ${'maiores'.$menor} = $qtde_dias;
      }
	  foreach($marcacoes as $k=>$v) {
         if ($v['qtde_dias'] == $maiores1 or $v['qtde_dias'] == $maiores2 or $v['qtde_dias'] == $maiores3) {
			$marcacoes[$k]['destaque'] = 'X';
		 }
	  }
	  echo json_encode($marcacoes);
	  //return json_encode($marcacoes);
   }
}
?>