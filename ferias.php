<?php
include_once("top.php");
?>
<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="dist/css/jquery.dataTables.min.css">
<style>
.destaque {
	background-color: #9ccfec !important;
}
</style>
<?php
include_once("header.php");
?>
      <!-- Full Width Column -->
      <div class="content-wrapper">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              Cálculo de Férias
            </h1>
          </section>

          <!-- Main content -->
          <section class="content">
            <div class="row">
               <div class="col-md-12">
			      <form role="form" NAME="form1">
                  <div class="box-body">
					<div class="form-group">
                      <label for="dias" class="col-sm-2 control-label" style="top:7px">Quantidade de Dias:</label>
                      <div class="col-sm-4">
                        <input type="number" class="form-control" id="dias" name="dias">
                      </div>
					  <div class="col-sm-4">
                        <button type="button" class="btn btn-primary" id="btnCalcular" name="btnCalcular">Calcular</button>
						<button type="button" class="btn btn-primary" id="btnCadastrar" name="btnCadastrar">Cadastrar Feriados</button>
                      </div>
                    </div>
                  </div><!-- /.box-body -->
                </form>
			   </div>
			   <div class="col-md-12" id="divResult" style="display:none;">
			    <div class="box">
					<div class="box-header">
						<h3 class="box-title">Resultado</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<table id="example2" class="table table-bordered table-hover stripe" cellspacing="0" width="100%">
						<thead>
						  <tr>
							<th><B>MELHOR DATA</B></th>
							<th><B>#</B></th>
							<th><B>DATA INÍCIO</B></th>
							<th><B>DATA FINAL</B></th>
							<th><B>QUANTIDADE DE DIAS</B></th>
							<th><B>DATA SAÍDA OFICIAL</B></th>
							<th><B>DATA RETORNO OFICIAL</B></th>
						  </tr>
						</thead>
						</table>
					</div>
				</div>
			   </div>
		    </div>
	      </section>
        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->
<?php
include_once("footer.php");
include_once("script_footer.php");
?>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function() {

	$("#btnCadastrar").click(function() {
		window.location = "cadastrarFeriado.php";
	});
	
	
	$("#btnCalcular").click(function() {
		console.log('entrou');
		var dias = $('#dias').val();
		console.log(dias);
		$.ajax({
			url: 'calculoFerias.php',
			method: 'POST',
			data:  {'dias': dias},
			dataType: "json",
			success: function(r){
				if (r != '') {
					$('#divResult').show();
					var table = $('#example2').DataTable( {
						'destroy': true,
						"data": r,
						"pageLength": 100,
						"columns": [
							{
							    "data": "destaque",
							    "visible": false
							},
							{ 
								"data": "indice",
							    "visible": false 
							},
							{ "data": "dt_inicio" },
							{ "data": "dt_final" },
							{ "data": "qtde_dias" },
							{ "data": "dt_saida_oficial" },
							{ "data": "dt_volta_oficial" }
						],
						"order": [[1, 'asc']],
						"language": {
								"sEmptyTable": "Nenhum registro encontrado",
								"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
								"sInfoEmpty": "Mostrando 0 até de 0 registros",
								"sInfoFiltered": "(Filtrados de _MAX_ registros)",
								"sInfoPostFix": "",
								"sInfoThousands": ".",
								"sLengthMenu": "_MENU_ resultados por página",
								"sLoadingRecords": "<img src='../img/carregando.gif'>",
								"sProcessing": "Processando...",
								"sZeroRecords": "Nenhum registro encontrado",
								"sSearch": "Pesquisar",
								"oPaginate": {
									"sNext": "Próximo",
									"sPrevious": "Anterior",
									"sFirst": "Primeiro",
									"sLast": "Último"
								},
								"oAria": {
									"sSortAscending": ": Ordenar colunas de forma ascendente",
									"sSortDescending": ": Ordenar colunas de forma descendente"
								}
						},
						"createdRow": function( row, data, dataIndex){
							if( data['destaque'] ==  'X'){
								$(row).addClass('destaque');
							}
						}
					});
				}
			},
			
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				alert('Erro ao calcular!');
				console.log(textStatus);
				console.log(XMLHttpRequest);
				console.log(errorThrown);
			}
		});
	});
});

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}
</script>
<?php
include_once("bottom.php");
?>      


